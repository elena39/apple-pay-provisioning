// swift-tools-version: 5.9
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "ApplePayProvisioning",
    platforms: [
        .iOS(.v14)
    ],
    products: [
        .library(
            name: "ApplePayProvisioning",
            targets: ["ApplePayProvisioning"]),
    ],
    targets: [
        .binaryTarget(
            name: "ApplePayProvisioning",
            url: "https://gitlab.com/phyreapp/phyre-apple-pay/-/blob/909e491af19cbbea91d101fd51aa217536b20c14/5.0.0/ApplePayProvisioning.zip",
            checksum: "58e63312e6f17487ec9ed9bc9ac018b90d7f20e1459b1d8a6a1548154adf31f2"),
    ]
)
